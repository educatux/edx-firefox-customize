## Customisation de FireFox 
Ce fichier supprime la barre d'Onglet au profit des modules :
- Sea Containers
- Tab Center Redux

### Modules intéressant

Voici la liste des modules installés sur mes FireFox - Vérifiés en Decembre 2018

- Amazon Container
- Close Window Button
- Containerise
- Dark Reader
- Framalink shortener
- Google Container
- HTTPS partout
- Incognito This Tab
- KeePassHttp-Connector
- Nouvelle page d'onglet par start.me
- Purple Private Windows
- Qwant
- Sea Containers
- Search google in Private
- Smart Referer
- Steam Database
- Suppression automatique des témoins
- Tab Center Redux
- uBlock Origin
- Winwdow Layout Resizer

### Customisation de l'interface avec userChrome.css

Dans le répertoire profile/ 
- créer un répertoire chrome
- copier : userChrome.css dans le répertoire chrome/